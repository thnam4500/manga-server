package models

type Manga struct {
	Id            int64  `gorm:"column:id"`
	Name          string `gorm:"column:name"`
	NameEn        string `gorm:"column:name_en"`
	FollowerCount int64  `gorm:"column:follower_count"`
	CreatedAt     int64  `gorm:"column:created_at"`
	UpdateAt      int64  `gorm:"column:updated_at"`
}

func (m *Manga) TableName() string {
	return "manga"
}
