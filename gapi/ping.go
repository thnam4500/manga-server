package gapi

import (
	"context"

	"github.com/thnam4500/manga/constants"
	"github.com/thnam4500/manga/pb"
)

func (s *Server) Ping(ctx context.Context, in *pb.PingMangaRequest) (*pb.PingMangaResponse, error) {
	return &pb.PingMangaResponse{
		Status:  constants.STATUS_RESPONSE_OK,
		Message: "Pong",
	}, nil
}
