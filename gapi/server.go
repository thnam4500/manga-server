package gapi

import (
	"github.com/redis/go-redis/v9"
	"github.com/thnam4500/manga/pb"
	"github.com/thnam4500/manga/store"
	"gorm.io/gorm"
)

type Server struct {
	redisDatabase *redis.Client
	store         store.Store
	pb.UnimplementedMangaServer
}

func NewServer(rdb *redis.Client, db *gorm.DB) *Server {
	server := &Server{redisDatabase: rdb}
	server.store = store.NewStore(db)
	return server
}
