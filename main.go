package main

import (
	"fmt"
	"net"

	"github.com/redis/go-redis/v9"
	"github.com/thnam4500/manga/gapi"
	"github.com/thnam4500/manga/pb"
	"google.golang.org/grpc"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func main() {
	rdb := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	defer rdb.Close()
	conn, err := net.Listen("tcp", ":9001")
	if err != nil {
		panic(err)
	}
	defer conn.Close()
	server := grpc.NewServer()

	dsn := "host=localhost user=postgres password=password dbname=manga_engine port=5432 sslmode=disable TimeZone=Asia/Ho_Chi_Minh"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	s := gapi.NewServer(rdb, db)
	pb.RegisterMangaServer(server, s)
	fmt.Println("Server is running on port 9001")
	if err := server.Serve(conn); err != nil {
		panic(err)
	}

}
