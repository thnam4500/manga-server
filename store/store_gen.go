package store

import (
	"gorm.io/gorm"
)

type GetMangaParams struct {
}

type Store interface {
}

type store struct {
	db *gorm.DB
}

func NewStore(db *gorm.DB) Store {
	return &store{db: db}
}
